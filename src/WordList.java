import java.util.ArrayList;

public class WordList {
    private ArrayList myList;

    public WordList(ArrayList listOfWords) {
        this.myList = listOfWords;
    }

    public int numWordsOfLength(int len) {
        int n = 0;

        for (int i = 0; i < this.myList.size(); i++) {
            if (this.myList.get(i).toString().length() == len) {
                n = n + 1;
            }
        }
        return n;
    }

    public void removeWordsOfLength(int len) {
        for (int i = this.myList.size() - 1; i > -1; i--) {
            if (this.myList.get(i).toString().length() == len) {
                this.myList.remove(i);
            }
        }
    }

    public String toString() {
        return this.myList.toString();
    }
}
